import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
    'Hawaii': 'Honolulu', 
}

state, capital = random.choice(list(capitals_dict.items()))
print(f"The state name is {state}")

correct = False

while (not correct):
    guess = input(f"Please guess what is the capital of {state}: ").capitalize()
    if (guess.lower() == "exit"):
        print("Goodbye")
        break
    if (capital != guess):
        print(f"{guess} is not the capital of {state}! Please try again")
        print(f"Type 'exit' to quit")
    else:
        print("Correct")
        break

#choose random state, then assign state and it's capital to two variables
#then display the state from random choice, and ask the user to guess it's capital.
#ask them repeatedly for it, until they get it right or type exit
#if they answer correctly, write "correct" and end it; if they type exit - "goodbye"
#change all to lower case letters, so input is fine whether wArSAW or warsaw  
