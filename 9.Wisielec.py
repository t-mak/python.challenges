zycia = 11
slowo = list(input("Proszę podać słowo do odgadnięcia: "))
for x in range(35):
    print("\n")

zaszyfrowane_slowo = []
for x in range(len(slowo)):
    zaszyfrowane_slowo.append("_")

def rysuj_wisielca(zycia):
    if zycia == 11:
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
    elif zycia == 10:
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("/            ")
    elif zycia == 9:
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("            ")
            print("/\\          ")
    elif zycia == 8:
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 7:
            print(" |---------  ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 6:
            print(" |---------  ")
            print(" |        |  ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 5:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |           ")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 4:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |        |  ")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 3:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |        |\\")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 2:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |       /|\\")
            print(" |           ")
            print(" |           ")
            print("/\\          ")
    elif zycia == 1:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |       /|\\")
            print(" |         \\")
            print(" |           ")
            print("/\\          ")
    elif zycia == 0:
            print(" |---------  ")
            print(" |        |  ")
            print(" |        O  ")
            print(" |       /|\\")
            print(" |       / \\")
            print(" |           ")
            print("/\\          ")

while (zycia > 0):
    print(f"Odgadywane slowo: {zaszyfrowane_slowo}")
    litera = input("Podaj literę: ")
    if (slowo.count(litera)): 
        print(f"\nLitera {litera} jest w słowie!\n")
        i = 0
        for znak in slowo:
            if znak == litera:
                zaszyfrowane_slowo[i] = slowo[i]
            i = i + 1
        rysuj_wisielca(zycia)
    else: 
        print("\nLitery nie ma w słowie!\n")
        zycia = zycia -1
        rysuj_wisielca(zycia)
    print("\n-----------------------------------------------------\n")
    if zaszyfrowane_slowo == slowo:
        break

print("***** KONIEC GRY *****")
if (zycia == 0):
    print("\nSłowo nieodgadnięte!")
    print(f"Słowo to {slowo}")
    rysuj_wisielca(zycia)
if (zycia > 0):
    print("\nSłowo odgadnięte!")
    print(f"Słowo to {slowo}")
    rysuj_wisielca(zycia)

"""
Zmienne:
słowo - do przechowania słowa. Pierwsza osoba je wymysla (input)
zycia - ile mozna bledow popelnic. Spada za kazdym razem jak sie poda literę,
ktorej nie ma
zycia spada do 0 - koniec gry, przegrane

Odgadywanie słowa:
//na poczatku same "_" np dla wybranego słowa Python "_ _ _ _ _ _"
Druga osoba wybiera litere, np "y"
Wyswietla sie, ze "Litera y znaleziona w słowie 1 raz"
Wysietla się "_ y _ _ _ _"
Druga osoba wybiera ponownie litere, np "a"
Wyswietla się "Nie znaleziono litery!"
Wyswietla się rysunek wisielca + 1 (na poczatek sama noga, potem druga itd).
"""
