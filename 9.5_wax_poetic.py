import random

def select_random(data, num):
    selected = []
    for x in range(num):
        selected.append(random.choice(data))
    return selected
        
nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

noun = select_random(nouns, 3)
ver = select_random(verbs, 3)
adj = select_random(adjectives, 3)
prep = select_random(prepositions, 2)
adv = select_random(adverbs, 1)

letter = adj[0][0:1]

if (letter == "a" or letter == "i" or letter == "u" or letter == "e" or letter == "o"):
    print(f"An {adj[0]} {noun[0]}\n")
else:
    print (f"A {adj[0]} {noun[0]}\n")

letter = adj[1][0:1]

if (letter == "a" or letter == "i" or letter == "u" or letter == "e" or letter == "o"):
    print(f"An {adj[0]} {noun[0]} {ver[0]} {prep[0]} the {adj[1]} {noun[1]}")
else:
    print(f"A {adj[0]} {noun[0]} {ver[0]} {prep[0]} the {adj[1]} {noun[1]}")

print(f"{adv[0]}, the {noun[0]} {ver[1]}")
print(f"the {noun[1]} {ver[2]} {prep[1]} a {adj[2]} {noun[2]}")

