#Dla uproszczenia niech komputer zamiast pionków ma cyfry, a gracz litery. Tutaj nie chodzi o zabawę z tworzeniem grafiki na ekranie, niech to będzie prosty interfejs tekstowy.

class Board:
    def __init__(self):
        self.board = ()
        self.placement = {}


    def initialize_game(self):
        
        self.board = (
            ('a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'),
            ('a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'),
            ('a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'),
            ('a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'),
            ('a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'),
            ('a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'),
            ('a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'),
            ('a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'))

        self.placement = {
            "AA" : "a1",
            "BB" : "c1",
            "CC" : "e1",
            "DD" : "g1",
            "EE" : "b2",
            "FF" : "d2",
            "GG" : "f2",
            "HH" : "h2",
            "II" : "a3",
            "JJ" : "c3",
            "KK" : "e3",
            "LL" : "g3",
            "01" : "b8",
            "02" : "d8",
            "03" : "f8",
            "04" : "h8", 
            "05" : "a7", 
            "06" : "c7", 
            "07" : "e7", 
            "08" : "g7", 
            "09" : "b6", 
            "10" : "d6", 
            "11" : "f6", 
            "12" : "h6",
        }       
