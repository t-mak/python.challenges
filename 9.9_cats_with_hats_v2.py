cats = []
round = 1 

def print_cats(cat_list):
    print("Lets print cats")
    index = 0
    for cat in cat_list:
        print(f"Cat number {(index)+1}: {cat}")
        index = index + 1
    print("***********************************\n")

while round <= 100:
    print(f"Runda {round}")
    if (round == 1):
        for cat in range(100):
            cats.append("Hat")
        print_cats(cats)
    else:
        for i in range (100):
            if (not ((i+1) % round)):
                if (cats[i] == "Hat"):
                    cats[i] = "No hat"
                else:
                    cats[i] = "Hat"
                print(f"cat number {i+1} has value {cats[i]}")
    round = round + 1
    
print_cats(cats)

"""
NOT (0+1 % 2) // NOT (1 % 2) // NOT 1 // False
NOT (1+1 % 2) // NOT (2 % 2) // NOT 0 // True

cat 3
Rounds
1 - hat
3 - no hat

cat 50
Rounds
1 - hat
2 - no hat
5 - hat
10 - no hat
25 - hat
50 - no hat

cat 90
Rounds
1 - hat
2 - no hat
3 - hat
5 - no hat
6 - hat
9 - no hat
10 - hat
15 - no hat
18 - hat
30 - no hat
45 - hat
90 - no hat
"""
